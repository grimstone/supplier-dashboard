<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Arrayhelper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use backend\models\OrdersStock;
use backend\models\Stock;
use backend\modules\Users\models\Users;

class FeedController extends Controller
{

    public $enableCsrfValidation = false;
    public $layout = false;

    public static $itoken = 'lsYtKjzGDDYHHB9FHjsrxiY5z2aIlOIF';

    // public function beforeAction($event)
    // {
    //     if ($_POST['itoken'] !== $this::$itoken && $_GET['itoken'] !== $this::$itoken)
    //     {
    //         Yii::$app->response->setStatusCode(403);
    //         Yii::$app->response->content = 'Access denied';
    //         return;
    //     }

    //     return parent::beforeAction($event);
    // }

    public function actionMagentoCreate()
    {

        $order = Yii::$app->request->post();
        Yii::info('create order', 'debug');
        $m = new OrdersStock();
        $models = [];
        $valuesToInsert = [];
        $num = Yii::$app->db->createCommand("SELECT MAX(uin) as max FROM orders_stock")->queryOne();
        $uin = (int)$num['max'] + 1;

        // create buyer in buyers table (if not exist)

        $m->iref = $order['iref'];
        $m->isMagento = true;

        $m->addBuyerByName(trim($order['buyer']), trim($order['buyer_name']));
        Yii::info('all good', 'debug');
        Yii::info('Is items empty?'.count($order['items']), 'debug');
        Yii::info(var_dump($order['items']), 'debug');
        foreach($order['items'] as $item)
        {
        	
            $model = new OrdersStock();
            if($order['is_mage2'] == true){
                $m2prefix = 'MAGE2_ORDER_';
            }else{
                $m2prefix = '';
            }

            if($order['is_mage2'] == true){
            	Yii::info('warehouse_ping: '.$item['warehouses'], 'debug');
                $model->shipped_in_detail = strtoupper($item['warehouses']);
            }

            $model->iref = $m2prefix.$order['iref'];
            $model->date_ordered = $order['date_ordered'];
            $model->buyer = $order['buyer'];
            $model->buyer_name = $order['buyer_name'];
            $model->buyer_id = $m->buyer_id;
            $model->buyer_country = $order['buyer_country'];
            $model->payment_method = $order['payment_method'];

            $model->seller = $item['seller'];
            $model->product_name = $item['product_name'];
            $model->description = $item['description'];
            $model->iar = $item['iar'];
            $model->quantity_ordered = (int)$item['quantity_ordered'];
            $model->real_quantity = $model->quantity_ordered * (-1);
            $model->unit_price = $item['unit_price'];
            $model->stock_value = floatval($model->real_quantity) * floatval($item['unit_price']);
            $model->order_amount = floatval($model->quantity_ordered) * floatval($item['unit_price']);

            $model->magento_currency = $order['magento_currency'];
            $model->magento_vat = $item['magento_vat'];


            $model->magento_price_and_vat = $item['magento_price_and_vat'];
            $model->magento_total_paid = $item['qty_invoiced'] * floatval($item['unit_price']);
            $model->magento_total_refunded = $item['magento_total_refunded'];
            if($item['unit_price'] == 0 || $item['unit_price'] == '0' || $item['unit_price'] == NULL){
                $model->operation = 'Stock OUT - After Sale Replacement';
            }else{
                $model->operation = 'Stock OUT - to Customer';
            }

            $model->uin = $uin++;
            $model->created_at = date('Y-m-d H:i:s');

            $model->isMagento = true;
            $model->addUserByName(trim($item['seller']));
            //if($order['is_mage2'] == true){
                $model->comex_percentage = 0;
            //}else{
                $model->setComexMargin();
                $model->adjustAutomaticallyFields();
            //}
            
            // if($item['unit_price'] == 0 || $item['unit_price'] == '0' || $item['unit_price'] == NULL){
            //     $model->order_status = 'COMPLETED';
            //     $model->payment_status_to_chcos = 'COMPLETED';
            // }

            $models[] = $model;
            $valuesToInsert[] = array_values($model->attributes);
        }

        // add Discount Entry
        Yii::info('still all good', 'debug');
        if (abs($order['discount_amount']) > 0)
        {
            $model = new OrdersStock();
            // $model->scenario = 'additional_entry';
            $model->addDiscountEntry($order, $m->buyer_id, $uin);
            $models[] = $model;
            $valuesToInsert[] = array_values($model->attributes);
        }

        // add Shipping Entry

        if ($order['shipping_amount'] > 0)
        {
            $model = new OrdersStock();
            // $model->scenario = 'additional_entry';
            $model->addShippingEntry($order, $m->buyer_id, $uin);
            $models[] = $model;
            $valuesToInsert[] = array_values($model->attributes);
        }

        if ($m::validateMultiple($models))
        {
            $columnsToInsert = $m->attributes();

            $transaction = Yii::$app->db->beginTransaction();

            try
            {
                $numberAffectedRows = Yii::$app->db->createCommand()
                    ->batchInsert($m->tableName(), $columnsToInsert, $valuesToInsert)
                    ->execute();


                $transaction->commit();
            }

            catch (\Exception $e)
            {
                $transaction->rollBack();
                \backend\modules\LogEvents\components\helpers\LogItem::add('feed_transaction_canceled', 0, NULL, "Magento (" . $order['feed_source'] . ") failed to insert the order " . $order['iref'] . " | " . $e->getMessage());
                throw $e;
            }

            catch (\Throwable $e) {
                $transaction->rollBack();
                \backend\modules\LogEvents\components\helpers\LogItem::add('feed_transaction_canceled', 0, NULL, "Magento (" . $order['feed_source'] . ") failed to insert the order " . $order['iref'] . " | " . $e->getMessage());
                throw $e;
            }


            $isSuccess = ($numberAffectedRows === count($models));
            // Yii::info(print_r($valuesToInsert,1), 'debug');

            if ($isSuccess)
            {
                $message = "Magento (" . $order['feed_source'] . ") has successfully created the order " . $order['iref'];
                \backend\modules\LogEvents\components\helpers\LogItem::add('feed_create_order', 0, NULL, $message);

                // DECREASE STOCK IN DASHBOARD

                $items_iar = [];
                foreach($order['items'] as $item){ $items_iar[] = $item['iar'];}
                $m_stock = new Stock();
                $stock_updated = $m_stock->adjustStockBatch($items_iar);

                if ($stock_updated)
                {
                    // $message = "Magento (" . $order['feed_source'] . ") updated the stock according to the new order " . $order['iref'];
                    \backend\modules\LogEvents\components\helpers\LogItem::add('feed_update_stock_by_new_order', 0, NULL, $order['iref']);
                }
            }
            else
            {
                $transaction->rollBack();
                $message = "Magento (" . $order['feed_source'] . ") failed to insert the order " . $order['iref'] . ". Not all items was saved";
                \backend\modules\LogEvents\components\helpers\LogItem::add('feed_transaction_canceled', 0, NULL, $message);
            }

        }
        else
        {
            // NOT VALIDATED

            $message = "Magento (" . $order['feed_source'] . ") failed to insert the order " . $order['iref'] . ". Not validated data.";
            \backend\modules\LogEvents\components\helpers\LogItem::add('feed_not_validated', 0, NULL, $message);

            // foreach($models as $model){
            //     if($model->hasErrors()){
            //         Yii::info(print_r($model->getFirstErrors(),1), 'debug');
            //     }
            // }
        }
    }

    public function actionMagentoCancel()
    {

        $order = Yii::$app->request->post();
        if($order['is_mage2'] == true){
            $m2prefix = 'MAGE2_ORDER_';
        }
        try
        {

            $update = Yii::$app->db->createCommand('UPDATE orders_stock SET
                operation="CANCELED",
                real_quantity=0,
                unit_price=0,
                order_amount=0,
                comex_fees=0,
                financial_fees=0,
                amount_due_to_chcos=0,
                total1_payment_to_chcos=0,
                total2_payment_to_chcos=0,
                stock_value=0,
                order_status="CANCELED",
                outstanding_payment_to_chcos=0,
                payment_status_to_chcos="CANCELED"
            WHERE iref="' . $m2prefix.$order['iref'] . '"')->execute();
            Yii::info('UPDATE orders_stock SET
                operation="CANCELED",
                real_quantity=0,
                unit_price=0,
                order_amount=0,
                comex_fees=0,
                financial_fees=0,
                amount_due_to_chcos=0,
                total1_payment_to_chcos=0,
                total2_payment_to_chcos=0,
                stock_value=0,
                order_status="CANCELED",
                outstanding_payment_to_chcos=0,
                payment_status_to_chcos="CANCELED"
            WHERE iref="' . $m2prefix.$order['iref'] . '"', 'debug');
            if ($update)
            {
                $message = "Magento (" . $order['feed_source'] . ") canceled the order " . $order['iref'];
                \backend\modules\LogEvents\components\helpers\LogItem::add('feed_cancel_order', 0, NULL, $message);

                // set flag changed_by_external to cancel user changes

                $items_arr = Yii::$app->db->createCommand('SELECT id FROM orders_stock WHERE iref="' . $order['iref'] .'" and busy_state!=0 and busy_state IS NOT NULL')->queryAll();
                $busy_items = [];
                foreach ($items_arr as $item)
                {
                    $busy_items[] = $item['id'];
                }
                if (count($busy_items))
                {
                    $update = Yii::$app->db->createCommand("UPDATE orders_stock SET changed_by_external=1 WHERE id IN (" . implode(',', $busy_items) . ")")->execute();
                }

                // INCREASE STOCK IN DASHBOARD

                $items_iar = [];
                foreach($order['items'] as $item){ $items_iar[] = $item['iar'];}
                $m_stock = new Stock();
                $stock_updated = $m_stock->adjustStockBatch($items_iar);

                if ($stock_updated)
                {
                    // $message = "Magento (" . $order['feed_source'] . ") updated the stock according to the canceled order " . $order['iref'];
                    \backend\modules\LogEvents\components\helpers\LogItem::add('feed_update_stock_by_canceled_order', 0, NULL, $order['iref']);
                }

            }

        }

        catch (\Exception $e)
        {
            // $result->error = 'SQL insert error';
            // $result->message = $e->getMessage();
            throw $e;
        }
    }

    public function actionMagentoUpdate()
    {
        $order = Yii::$app->request->post();
        //Yii::info(print_r($order['items'][$row['iar']]['warehouses'], 1), 'debug');
		if($order['is_mage2'] == true){
            $m2prefix = 'MAGE2_ORDER_';
        }



        // payment_from_buyer_date is not included in the form
        // so when Magento will updated the entry it will not interfer with the user form data (during editing)

        $rows = Yii::$app->db->createCommand('SELECT *
            FROM orders_stock
            WHERE
                iref = "' . $m2prefix.$order['iref'] . '"
                AND (LOWER(TRIM(operation)) = "stock out - to customer"
                OR LOWER(TRIM(operation)) = "stock out - after sale replacement")
                AND is_additional IS NULL
        ')->queryAll();
        
        //Yii::info('is there warehouse? '.count($rows), 'debug');
        
        if (count($rows))
        {
             //Yii::info(print_r($rows,1), 'debug');
            $update_qty_invoiced_str = "";
            $update_payment_from_buyer_date_str = "";
            $update_total_paid_str = "";
            $update_payment_status_to_chcos_str = "";
            $update_warehouse_str = "";
            $update_where_str = [];


            //Yii::info('start update '.$i, 'debug');
            foreach($rows as $row)
            {
                    if(isset($order['transmission'])){
                        Yii::$app->db->createCommand("
                            UPDATE orders_stock
                            SET transmission = '" . $order['transmission'] . "'
                            WHERE
                                iref = '" . $order['iref'] . "'
                                AND iar = '" . $row['iar'] . "'
                                AND is_additional IS NULL
                                AND shipped_in_detail = 'PANALPINA'
                        ")->execute();
                    }
                    $qty_invoiced = (int)$order['items'][$row['iar']]['qty_invoiced'];
                    $total_paid = $qty_invoiced * floatval($row['unit_price']);

                    $outstanding = $row['outstanding_payment_to_chcos'];
                    $order_status = strtolower(trim($row['order_status']));

                    $operation = strtolower(trim($row['operation']));

                    $payment_status = 'PENDING';
                    //what what operations system able to change status
                    $enableddOperations = array('stock out - to supplier', 'stock out - to customer', 'stock out - after sale replacement', 'stock in - after sale return');
                    if(in_array($operation, $enableddOperations)){
                    	// Yii::info('payment_status is able to change for order n#'.$order['iref'].' cuz its operation is: '.$operation, 'debug');
                    	if($order_status == 'in progress' || $order_status == 'in transit')
	                    {
	                        //Yii::info('we here, magento vat is '.($total_paid+floatval($row['magento_vat'])), 'debug');
	                       // $total_paid += floatval($row['magento_vat']);
	                        $payment_status = 'PENDING';
	                    }else if($order_status == 'completed' && $outstanding != 0){
	                        //Yii::info('or here: ', 'debug');
	                        $payment_status = 'IN PROGRESS';
	                    }else if($order_status == 'completed' && $outstanding == 0){
	                        //Yii::info('or maybe here: ', 'debug');
	                        //$total_paid += floatval($row['magento_vat']);
	                        $payment_status = 'COMPLETED';
	                    }
                    }else{
                    	// Yii::info('payment_status is not able to change for order n#'.$order['iref'].' cuz its operation is: '.$operation, 'debug');
                    	$payment_status = $row['payment_status'];
                    	
                    }
	                    
                    //removed block following new payment and order statuses rules
                    // $payment_status = 'IN PROGRESS';
                    //    if(in_array($operation, $enableddOperations)){
	                //     if ($qty_invoiced == (int)$row['quantity_ordered'])
	                //     {
	                //         $total_paid += floatval($row['magento_vat']);
	                //         $payment_status = 'COMPLETED';
	                //     }
	                // }
                    if($order['items'][$row['iar']]['warehouses'] == 'ZEESHOP' || $order['items'][$row['iar']]['warehouses'] == ''){
                        Yii::$app->db->createCommand("
                            UPDATE orders_stock
                            SET shipped_in_detail = 'ZEESHOP'
                            WHERE
                                iref = '" . $order['iref'] . "'
                                AND is_additional IS NOT NULL
                                AND is_additional != 2
                        ")->execute();
                    }
                    if (($qty_invoiced > 0 && $qty_invoiced > (int)$row['magento_qty_invoiced']) || $order['items'][$row['iar']]['warehouses'] != $row['shipped_in_detail'])
                    {
                        if ($qty_invoiced > 0 && $qty_invoiced > (int)$row['magento_qty_invoiced'])
                        {
                            $update_payment_from_buyer_date_str .= " WHEN '" . $row['id'] . "' THEN '" . date('Y-m-d H:i:s') . "' ";
                        }
                        else
                        {
                            $update_payment_from_buyer_date_str .= " WHEN '" . $row['id'] . "' THEN magento_payment_from_buyer_date ";
                        }



                        $update_qty_invoiced_str .= " WHEN '" . $row['id'] . "' THEN '" . $qty_invoiced . "' ";
                        $update_total_paid_str .= " WHEN '" . $row['id'] . "' THEN '" . $total_paid . "' ";
                        $update_payment_status_to_chcos_str .= " WHEN '" . $row['id'] . "' THEN '" . $payment_status . "' ";

                        $update_warehouse_str .= " WHEN '" . $row['id'] . "' THEN '" . strtoupper($order['items'][$row['iar']]['warehouses']) . "' ";

                        $update_where_str[] = $row['id'];
                    }

                                // updating stock via 1018
                                Yii::info($update_warehouse_str, 'debug');
                                $rightQ = Yii::$app->db->createCommand("SELECT SUM(real_quantity) AS quantity
                                    FROM orders_stock
                                    WHERE iar='" . $row['iar'] . "' AND (shipped_in_detail != 'ZEESHOP' OR shipped_in_detail IS NULL)
                                ")->queryOne();
                                if($rightQ){
                                   $update = Yii::$app->db->createCommand("
                                    UPDATE stock
                                    SET qty=" . $rightQ['quantity'] . " ,
                                        stock_value = qty*unit_price,
                                        updated_at = '" . date('Y-m-d H:i:s') . "'
                                    WHERE iar='" . $row['iar'] . "'
                                    ")->execute();
                                    // Yii::info('Stock\'s value is:'.$rightQ['quantity'], 'debug');
                                }else{
                                    // Yii::info('Stock\'s updating error', 'debug');
                                }


                                // updating stock via 1018

            }

            if ($order['total_due'] == 0 && $order['total_paid'] > 0)
            {
                $payment_from_buyer_date = date('Y-m-d H:i:s');

                Yii::$app->db->createCommand("
                    UPDATE orders_stock
                    SET magento_total_paid = magento_price_and_vat,
                        updated_at = '" . date('Y-m-d H:i:s') . "'
                    WHERE
                        iref = '" . $order['iref'] . "'
                        AND is_additional IS NOT NULL
                ")->execute();
            }else{
                $payment_from_buyer_date = "0000-00-00";
            }
            if (count($update_where_str))
            {
                $update = Yii::$app->db->createCommand("
                UPDATE orders_stock
                SET magento_qty_invoiced = (CASE id $update_qty_invoiced_str END),
                magento_payment_from_buyer_date = (CASE id $update_payment_from_buyer_date_str END),
                magento_total_paid = (CASE id $update_total_paid_str END),
                payment_status_to_chcos = (CASE id $update_payment_status_to_chcos_str END),
                shipped_in_detail = (CASE id $update_warehouse_str END),
                payment_from_buyer_date = '" . $payment_from_buyer_date . "',
                updated_at = '" . date('Y-m-d H:i:s') . "'
                WHERE id IN('" . implode("','", $update_where_str) . "')
                ")->execute();
                // Yii::info('tp_string '.$update_total_paid_str, 'debug');
            }
        }



        // if ($order['total_due'] == 0 && $order['total_paid'] > 0)
        // {
        //     $update = Yii::$app->db->createCommand('UPDATE orders_stock
        //         SET payment_from_buyer_date="' . date('Y-m-d H:i:s') . '"
        //         WHERE iref="' . $order['iref'] . '" and payment_from_buyer_date IS NULL
        //     ')->execute();
        // }

        if ($update)
        {
            // set flag changed_by_external to cancel user changes

            $items_arr = Yii::$app->db->createCommand('SELECT id FROM orders_stock WHERE iref="' . $order['iref'] .'" and busy_state!=0 and busy_state IS NOT NULL')->queryAll();
            $busy_items = [];
            foreach ($items_arr as $item)
            {
                $busy_items[] = $item['id'];
            }
            if (count($busy_items))
            {
                $update_flag = Yii::$app->db->createCommand("UPDATE orders_stock SET changed_by_external=1 WHERE id IN (" . implode(',', $busy_items) . ")")->execute();
            }

            \backend\modules\LogEvents\components\helpers\LogItem::add('feed_set_payment_date', 0, NULL, $order['iref']);
        }

    }

    public function actionMagentoStockUpdate()
    {
        $product = Yii::$app->request->post();
        // Yii::info(print_r($product,1), 'debug');
        try
        {
            $stock_value = floatval($product['qty']) * floatval($product['unit_price']);

            // here we don't update qty. Qty is calculated within the SupplierDashboard
            $update = Yii::$app->db->createCommand('UPDATE stock
                SET sar="' . $product['sar'] . '", product_name=' . Yii::$app->db->quoteValue($product['product_name']) . ', min_qty=' . (isset($product['min_qty']) ? $product['min_qty'] : 'min_qty') . ',
                    unit_price="' . $product['unit_price'] . '", stock_value=qty*unit_price, updated_at="' . date('Y-m-d H:i:s') . '"
                WHERE iar="' . $product['iar'] . '"
            ')->execute();

            if ($update)
            {
                $message = "Magento (" . $product['feed_source'] . ") updated the stock: " . $product['product_name'] . " (IAR: " . $product['iar'] . ")";
                \backend\modules\LogEvents\components\helpers\LogItem::add('feed_update_stock', 0, NULL, $message);
                Yii::info('product updated', 'debug');
            }
            else
            {

                // CREATE NEW STOCK

                if (Yii::$app->db->createCommand('SELECT COUNT(*) FROM stock WHERE iar="' . $product['iar'] . '"')->queryScalar() == 0)
                {
                    Yii::$app->db->createCommand('INSERT INTO stock (product_name, sar, iar, min_qty, qty, unit_price, stock_value, created_at, visible)
                        VALUES (
                            ' . Yii::$app->db->quoteValue($product['product_name']) . ',
                            "' . $product['sar'] . '",
                            "' . $product['iar'] . '",
                            "' . $product['min_qty'] . '",
                            "' . $product['qty'] . '",
                            "' . $product['unit_price'] . '",
                            "' . $stock_value . '",
                            "' . date('Y-m-d H:i:s') . '",
                            1
                        )
                    ')->execute();

                    $message = "Magento (" . $product['feed_source'] . ") add stock: " . $product['product_name'] . " (IAR: " . $product['iar'] . ")";
                    \backend\modules\LogEvents\components\helpers\LogItem::add('feed_add_stock', 0, NULL, $message);
                    Yii::info('product added', 'debug');
                }
            }

        }

        catch (\Exception $e)
        {
            // $result->error = 'SQL insert error';
            // $result->message = $e->getMessage();
            throw $e;
        }
    }

    public function actionMagentoStockDelete()
    {
        $product = Yii::$app->request->post();

        $delete = Yii::$app->db->createCommand('DELETE FROM stock WHERE iar="' . $product['iar'] . '"')->execute();

        if ($delete)
        {
            Yii::info('product deleted'.$product['iar'], 'debug');
            $message = "Magento (" . $product['feed_source'] . ") delete stock: " . $product['product_name'] . " (IAR: " . $product['iar'] . ")";
            \backend\modules\LogEvents\components\helpers\LogItem::add('feed_delete_stock', 0, NULL, $message);
        }
    }

    public function actionCronOrderStatus()
    {
        // don't matter if an Entry is editing because after editing (save) the result of order_status will be properly (recalculated)
        // so we don't need to block the Entry
        
        $rows = Yii::$app->db->createCommand('SELECT id, iref, iar, operation, order_status, payment_method, outstanding_payment_to_chcos
            FROM orders_stock
            WHERE
                (TIMESTAMPDIFF(DAY, day_shipping,NOW()) >= 20)
                AND order_status != "COMPLETED" AND order_status != "CANCELED"
                AND operation = "stock out - to customer"
                AND quantity_ordered = real_quantity*(-1)
        ')->queryAll();

        // $rows = Yii::$app->db->createCommand('SELECT id, iref, iar, order_status
        //     FROM orders_stock
        //     WHERE
        //         (TIMESTAMPDIFF(HOUR, delivered_date,NOW()) >= 48)
        //         AND order_status != "COMPLETED" AND order_status != "CANCELED"
        //         AND operation = "stock out - to customer"
        //         AND quantity_ordered = real_quantity*(-1)
        // ')->queryAll();

        if (count($rows))
        {
            $update_order_status_str = "";
            $update_payment_status_str = "";
            $update_where_str = [];
            $log_batch = [];
            foreach($rows as $row)
            {
            	$enableddOperations = array('stock out - to supplier', 'stock out - to customer', 'stock out - after sale replacement', 'stock in - after sale return');
                if(in_array(strtolower($row['operation']), $enableddOperations)){
	                if($row['outstanding_payment_to_chcos'] > 0){
	                    $payment_status = 'IN PROGRESS';
	                }else if($row['outstanding_payment_to_chcos'] == 0){
	                    $payment_status = 'COMPLETED';
	                }
	            }else{
	            	$payment_status = $row['payment_status'];
	            }
                $update_order_status_str .= " WHEN '" . $row['id'] . "' THEN 'COMPLETED' ";
                $update_payment_status_str .= " WHEN '" . $row['id'] . "' THEN '" . $payment_status . "'";
                $update_where_str[] = $row['id'];
                $log_batch[] = [0, NULL, $row['iref'] . '(' . $row['iar'] . ') - COMPLETED'];
            }

            $update = Yii::$app->db->createCommand("
                UPDATE orders_stock
                SET order_status = (CASE id $update_order_status_str END),
                    payment_status_to_chcos = (CASE id $update_payment_status_str END),
                    updated_at = '" . date('Y-m-d H:i:s') . "'
                WHERE id IN('" . implode("','", $update_where_str) . "');
            ")->execute();

            if ($update)
            {
                //Yii::info(print_r('Done, payment_update_str is: '.$update_payment_status_str, 1), 'debug');
                \backend\modules\LogEvents\components\helpers\LogItem::addBatch('system_update_order_status', $log_batch);
            }
        }
        $this->actionUpdateOrderStatusSixteen();
        $this->actionUpdateOrderStatusThirty();
    }

    public function actionMagentoSellerUpdate()
    {
        $company = Yii::$app->request->post('seller');

        if (trim($company['sku'])!=='')
        {

            if (Yii::$app->db->createCommand('SELECT COUNT(*) FROM users WHERE user_code="' . $company['sku'] . '"')->queryScalar() == 0)
            {
                // INSERT

                if (trim($company['title'])!=='' && trim($company['brand_name'])!=='' && trim($company['email'])!=='' && trim($company['username'])!=='' && trim($company['password'])!=='')
                {
                    $user = new Users();
                    $user->role = 'seller';
                    $user->scenario = 'seller_scenario';
                    $user->user_code = $company['sku'];
                    $user->username = $user->prettyUsername($company['username']);
                    $user->user_name = $company['title'];
                    $user->user_surname = $company['brand_name'];
                    $user->comex_margin = (trim($company['comex_margin'])!='') ? trim($company['comex_margin']) : NULL;
                    $user->email = $company['email'];
                    $user->setPassword($company['password']);
                    $user->generateAuthKey();

                    if ($user->save())
                    {
                        $user->sendNewSellerAccountEmailToEST($company['password']);
                        \backend\modules\LogEvents\components\helpers\LogItem::add('magento_supplier_add', 0, NULL, $company['title'] . ' (' . $company['brand_name'] . ')');
                    }
                    else
                    {
                        \backend\modules\LogEvents\components\helpers\LogItem::add('magento_supplier_add_error', 0, NULL, $company['title'] . ' (' . $company['brand_name'] . ')');
                    }
                }
                else
                {
                    echo 'Please complete all mandatory fields.';
                }
            }
            else
            {
                // UPDATE

                if (trim($company['email'])!=='')
                {
                    $user = Users::findOne(['user_code' => trim($company['sku'])]);

                    $update = Yii::$app->db->createCommand("
                        UPDATE users
                        SET username=" . Yii::$app->db->quoteValue($company['username']) . ",
                            password_hash=" . Yii::$app->db->quoteValue(Yii::$app->security->generatePasswordHash($company['password'])) . ",
                            user_name=" . Yii::$app->db->quoteValue($company['title']) . ",
                            user_surname=" . Yii::$app->db->quoteValue($company['brand_name']) . ",
                            comex_margin=" . (trim($company['comex_margin'])!='' ? floatval(trim($company['comex_margin'])) : "NULL") . ",
                            email='" . $company['email'] . "',
                            updated_at = '" . time() . "'
                        WHERE user_code='" . trim($company['sku']) . "'
                    ")->execute();

                    $update2 = Yii::$app->db->createCommand("
                        UPDATE orders_stock
                        SET seller=" . Yii::$app->db->quoteValue($company['title']) . "
                        WHERE user_id='" . $user['id'] . "'
                    ")->execute();

                    if ($update && $update2)
                    {
                        \backend\modules\LogEvents\components\helpers\LogItem::add('magento_supplier_update', 0, NULL, $company['title'] . ' (' . $company['brand_name'] . ')');
                    }
                    else
                    {
                        \backend\modules\LogEvents\components\helpers\LogItem::add('magento_supplier_update_error', 0, NULL, $company['title'] . ' (' . $company['brand_name'] . ')');
                        echo 'query update error';
                    }
                }
            }


        }
        else
        {
            echo 'Reference (seller code) is not defined.';
        }


    }

    public function actionMagentoShipmentCreate()
    {

        $shipment = Yii::$app->request->post();

        if($shipment['is_mage2'] == true){
            $shipment['order_id'] = 'MAGE2_ORDER_'.$shipment['order_id'];
        }
        if(isset($shipment['transmission'])){
            $transmission = $shipment['transmission'];
        }
        if (isset($shipment['items']))
        {
            $update_day_shipping_str = "";
            $update_error_str = "";
            $update_where = [];

            foreach($shipment['items'] as $item)
            {
                $update_error_str        .= " WHEN iar='" . $item['sku'] . "' AND (quantity_ordered!='" . (int)$item['qty'] . "' OR (UNIX_TIMESTAMP(day_shipping)!=0 AND day_shipping IS NOT NULL)) THEN 'ERROR' ";
                $update_day_shipping_str .= " WHEN iar='" . $item['sku'] . "' AND (quantity_ordered='" . (int)$item['qty'] . "' AND ((UNIX_TIMESTAMP(day_shipping)=0 OR UNIX_TIMESTAMP(day_shipping) IS NULL) AND day_shipping NOT LIKE 'ERROR' OR day_shipping IS NULL)) THEN '" . $shipment['created_at'] . "' ";

                $update_where[] = $item['sku'];
            }

            $update_err = Yii::$app->db->createCommand("UPDATE orders_stock
                SET day_shipping = CASE $update_error_str ELSE day_shipping END
                WHERE iref='" . $shipment['order_id'] . "' AND iar IN('" . implode("','", $update_where) . "')
            ")->execute();

            $update = Yii::$app->db->createCommand("UPDATE orders_stock
                SET day_shipping = CASE $update_day_shipping_str ELSE day_shipping END
                WHERE iref='" . $shipment['order_id'] . "' AND iar IN('" . implode("','", $update_where) . "')
            ")->execute();

            Yii::info("UPDATE orders_stock
                SET day_shipping = CASE $update_day_shipping_str ELSE day_shipping END
                WHERE iref='" . $shipment['order_id'] . "' AND iar IN('" . implode("','", $update_where) . "')
            ", 'debug');
            
            $dayShipping = $shipment['created_at'];

            if($dayShipping){
                if((strtotime('now')-strtotime($dayShipping))/(60*60*24) <= 20){// last formula due #1240
                    $orderStatus = 'IN TRANSIT';
                }else{
                    $orderStatus = 'COMPLETED';
                }
            }

            $update2 = Yii::$app->db->createCommand("UPDATE orders_stock SET transmission = '".$transmission."', order_status = '".$orderStatus."' WHERE shipped_in_detail = 'PANALPINA' AND iref='" . $shipment['order_id'] . "' AND iar IN('" . implode("','", $update_where) . "')
            ")->execute();

        }

        if ($update_err)
        {
            \backend\modules\LogEvents\components\helpers\LogItem::add('feed_shipment_create_set_error', 0, NULL, $shipment['order_id']);
        }
        if ($update)
        {

            \backend\modules\LogEvents\components\helpers\LogItem::add('feed_shipment_create_set_day_shipping', 0, NULL, $shipment['order_id']);
        }
        Yii::info('succesfully saved shipment from mage2', 'debug');
        // Yii::info(print_r($shipment, 1), 'debug');
    }

    public function actionMagentoShipmentUpdate()
    {
        $shipment = Yii::$app->request->post();
        if($shipment['is_mage2'] == true){
            $shipment['order_id'] = 'MAGE2_ORDER_'.$shipment['order_id'];
        }

        Yii::info(print_r($shipment, 1), 'debug');

        if (isset($shipment['items']))
        {
            $update = Yii::$app->db->createCommand("UPDATE orders_stock
                SET carrier='" . $shipment['carrier'] . "', tracking='" . $shipment['tracknumber'] . "'
                WHERE iref='" . $shipment['order_id'] . "' AND iar IN('" . implode("','", $shipment['items']) . "')
            ")->execute();
            Yii::info('succesfully updated tracking information for order #'.$shipment['order_id'], 'debug');
        }

        if ($update)
        {
            \backend\modules\LogEvents\components\helpers\LogItem::add('feed_shipment_add_trackings', 0, NULL, $shipment['order_id']);

        }

        // Yii::info(print_r($shipment, 1), 'debug');
    }

    public function actionChangeWarehouse()
    {
        $warehouses = Yii::$app->request->post();
        if($warehouses['is_mage2'] == true){
            $m2prefix = 'MAGE2_ORDER_';
        }else{
            $m2prefix = '';
        }

        if (!empty($warehouses))
        {
            $update = Yii::$app->db->createCommand("UPDATE orders_stock
                SET shipped_in_detail='" . $warehouses['new_warehouse'] . "' WHERE iref='" . $m2prefix.$warehouses['order_id'] . "' AND iar='" . $warehouses['sku'] ."'")->execute();
            Yii::info("UPDATE orders_stock
                SET shipped_in_detail='" . $warehouses['new_warehouse'] . "' WHERE iref='" . $m2prefix.$warehouses['order_id'] . "' AND iar='" . $warehouses['sku'] ."'", 'debug');
        }

        if ($update)
        {
            $msg = 'Successfully changed warehouse for order #'.$warehouses['order_id'].' ('.$warehouses['old_warehouse'].' -> '.$warehouses['new_warehouse'].')';
            \backend\modules\LogEvents\components\helpers\LogItem::add('change_warehouses', 0, NULL, $msg);

        }else{
            $msg = 'Error during change warehouse for order '.$warehouses['order_id'].' ('.$warehouses['old_warehouse'].' -> '.$warehouses['new_warehouse'].')';
            \backend\modules\LogEvents\components\helpers\LogItem::add('change_warehouses_error', 0, NULL, $msg);
        }
        //Yii::info(print_r($warehouses, 1), 'debug');
    }










    public function actionCreateCreditMemo()
    {
        $creditMemo = Yii::$app->request->post();
        

        $m = new OrdersStock();
        $models = [];
        $valuesToInsert = [];

        $num = Yii::$app->db->createCommand("SELECT MAX(uin) as max FROM orders_stock")->queryOne();
        $uin = (int)$num['max'] + 1;
        
        Yii::info(print_r($creditMemo, 1), 'debug');
        Yii::info('we start update credit memo'.print_r($orderId, 1), 'debug');
        Yii::info(var_dump($order), 'debug');
        if($creditMemo['is_mage2'] == true){
            $m2prefix = 'MAGE2_ORDER_';
        }else{
            $m2prefix = '';
        }

        $orderId = $m2prefix.$creditMemo['order_id'];

        // getting old data
        
        $originOrder = OrdersStock::find()
                ->where(['iref' => $orderId])
                ->one();

        foreach($creditMemo['items'] as $sku=>$item)
        {

            $oldEntry = OrdersStock::find()
                ->where(['iar' => $sku])
                ->one();



            $model = new OrdersStock();
            $model->iref = $orderId;
            $model->date_ordered = $originOrder['date_ordered'];
            $model->buyer = $originOrder['buyer'];
            $model->buyer_name = $originOrder['buyer_name'];
            $model->buyer_id = $originOrder['buyer_id'];
            $model->buyer_country = $originOrder['buyer_country'];
            $model->payment_method = $originOrder['payment_method'];
            $model->user_id = $oldEntry['user_id'];
            $model->seller = $oldEntry['seller'];
            $model->product_name = $item['product_name'];
            $model->description = $oldEntry['description'];
            $model->iar = $sku;
            $model->quantity_ordered = -1*$item['qty'];
            $model->real_quantity = $item['qty'];
            $model->unit_price = $item['unit_price'];
            $model->stock_value = $item['stock_value'];
            $model->shipped_in_detail = $item['warehouse'] == 'ZEESHOP' ? 'ZEESHOP' : 'ERROR';
            $model->operation = 'Stock IN - After Sale Return';
            $model->uin = $uin++;
            $model->created_at = date('Y-m-d H:i:s');


            $comexMarginPercentage = Users::find('comex_margin')
                ->where(['id' => $oldEntry['user_id']])
                ->one();
            if($comexMarginPercentage){
                $comexMargin = $item['stock_value'] * $comexMarginPercentage['comex_margin']/100;
            }else{
                $comexMarginPercentage = -1;
                $comexMargin = $item['stock_value'] * $comexMarginPercentage['comex_margin']/100;
            }
            $model->comex_fees = $comexMargin * -1;
            //Yii::info(print_r($comexMargin * -1, 1), 'debug');

            $model->adjustAutomaticallyFields();
            $model->order_status = 'COMPLETED';
            $model->payment_status_to_chcos = 'IN PROGRESS';
            $models[] = $model;
            $valuesToInsert[] = array_values($model->attributes);
        }

        if ($m::validateMultiple($models))
        {
            $columnsToInsert = $m->attributes();

            $transaction = Yii::$app->db->beginTransaction();

            try
            {
                $numberAffectedRows = Yii::$app->db->createCommand()
                    ->batchInsert($m->tableName(), $columnsToInsert, $valuesToInsert)
                    ->execute();


                $transaction->commit();
            }

            catch (\Exception $e)
            {
                $transaction->rollBack();
                throw $e;
            }

            catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

            $isSuccess = ($numberAffectedRows === count($models));

            if ($isSuccess)
            {
                $message = "Magento has successfully created credit memo for order " . $creditMemo['order_id'];
                \backend\modules\LogEvents\components\helpers\LogItem::add('feed_create_credit_memo', 0, NULL, $message);

                // DECREASE STOCK IN DASHBOARD

                $items_iar = [];
                foreach($creditMemo['items'] as $key=>$item){ $items_iar[] = $key;}
                $m_stock = new Stock();
                $stock_updated = $m_stock->adjustStockBatch($items_iar);

                if ($stock_updated)
                {
                    $message = "Stock updated due credit memo for order " . $creditMemo['order_id'];
                    \backend\modules\LogEvents\components\helpers\LogItem::add('feed_update_stock_by_credit_memo', 0, NULL, $message);
                }
            }
            else{
                $message = "Error with credit memo update " . $creditMemo['order_id'];
                \backend\modules\LogEvents\components\helpers\LogItem::add('feed_create_credit_memo', 0, NULL, $message);
            }
        }


    }










// SERVICE FUNCTIONS
// bugfixes, updates and etc

    public function actionTestsellersupdate()
    {
        $data['itoken'] = \backend\controllers\FeedController::$itoken;

        $data['sellers'] = Yii::$app->db->createCommand("SELECT username, email, user_code FROM users
            WHERE role='seller'
        ")->queryAll();

        $data['sellers'] = \yii\helpers\ArrayHelper::index($data['sellers'], 'user_code');

        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, Yii::$app->params['euMagentoURL'] . '/supplier-dashboard/index/updateallsellers');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $out = curl_exec($curl);

            curl_close($curl);
        }
    }

    public function actionUpdateCompanyOrders()
    {
        $_POST['itoken'] = \backend\controllers\FeedController::$itoken;

        $data['itoken'] = \backend\controllers\FeedController::$itoken;

        // curl_setopt($curl, CURLOPT_URL, Yii::$app->params['euMagentoURL'] . '/supplier-dashboard/index/getorders');
         if( $curl = curl_init() && (1 == 2) ) {

            curl_setopt($curl, CURLOPT_URL, Yii::$app->params['euMagentoURL'] . '/supplier-dashboard/index/getorders');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $out = curl_exec($curl);

            curl_close($curl);

            $customerOrderCompany = unserialize($out);

            foreach ($customerOrderCompany as $singleOrder)
                {
                    $update_buyer_str .= " WHEN '" . $singleOrder['id'] . "' THEN '".addslashes($singleOrder['company'])."' ";
                    $update_where_str[] = $singleOrder['id'];
                   // $log_batch[] = [0, NULL, $row['iref'] . '(' . $row['iar'] . ') - COMPLETED'];
                }

            $update1 = Yii::$app->db->createCommand("
                UPDATE orders_stock
                SET buyer = (CASE iref $update_buyer_str END),
                    updated_at = '" . date('Y-m-d H:i:s') . "'
                WHERE iref IN('" . implode("','", $update_where_str) . "');
            ")->execute();

            $items_arr = Yii::$app->db->createCommand("SELECT buyer_id, buyer FROM orders_stock WHERE iref IN('" . implode("','", $update_where_str) . "')")->queryAll();

            foreach ($items_arr as $item)
                {
                    $update_company_str .= " WHEN '" . $item['id'] . "' THEN '".addslashes($item['buyer'])."' ";
                    $update_company_where_str[] = $item['id'];
                   // $log_batch[] = [0, NULL, $row['iref'] . '(' . $row['iar'] . ') - COMPLETED'];
                }
                //echo $update_company_str;
            $update2 = Yii::$app->db->createCommand("
                UPDATE buyers
                SET name = (CASE id $update_company_str END)
                WHERE id IN('" . implode("','", $update_company_where_str) . "');
            ")->execute();

            echo 'success!';
        }
    }

    public function actionUpdateOrderStatus()
    {
        $orders2 = Yii::$app->db->createCommand('SELECT id, uin, payment_method, day_shipping, operation, order_status FROM orders_stock WHERE order_status != "CANCELED"')->queryAll();

        foreach ($orders2 as $key => $value) {
                echo $value['id'].' - '.$value['day_shipping'].' - ';
            //$value['day_shipping'] == '' || $value['day_shipping'] == 'ERROR' || $value['day_shipping'] == NULL
                $comp = true;
                if((int)$value['day_shipping'] != 0 && ($value['day_shipping'] != '' || $value['day_shipping'] != 'ERROR' || $value['day_shipping'] != NULL) ){
                    $fN = (strtotime('now')-strtotime($value['day_shipping']))/(60*60*24);
                    if($value['payment_method'] == '30'){
                        $countDays = 31;
                    }else if($value['payment_method'] == '60'){
                        $countDays = 61;
                    }else{
                        $countDays = 21;
                    }
                    //echo $countDays;
                    $comp = ((int)$fN <= $countDays);
                }

                $enableddOperations = array('stock out - to supplier', 'stock out - to customer', 'stock out - after sale replacement', 'stock in - after sale return');
                if(in_array(strtolower(trim($value['operation'])), $enableddOperations)){
	                if ($comp == true){
	                	
	                    if($value['day_shipping'] == '' || $value['day_shipping'] == 'ERROR' || $value['day_shipping'] == NULL || $value['day_shipping'] == '0000-00-00'){
	                		$correct = 'IN PROGRESS';
	                	}else{
	                    	$correct = 'IN TRANSIT';
	                	}

	                }
	                else{
	                    $correct = 'COMPLETED';
	                }
	            }else{
	            	$correct = '';
	            }
                echo $correct.'<br>';
                //echo ' - '.$correct.' - '.$value['day_shipping'].'<br>';
                if($value['order_status'] != $correct){
                    $update_order_str .= " WHEN '" . $value['id'] . "' THEN '".addslashes($correct)."' ";
                    $update_order_where_str[] = $value['id'];
                }
                echo 'correct value for '.$value['id'].' - '.$correct.'(day shipping is '.$value['day_shipping'].')<br>';
                //echo $update_order_str.'<br>';
                //echo $update_order_where_str.'<br>';
        }

        if($update_order_where_str && $update_order_str){
            $update2 = Yii::$app->db->createCommand("
                    UPDATE orders_stock
                    SET order_status = (CASE id $update_order_str END)
                    WHERE id IN('" . implode("','", $update_order_where_str) . "');
                ")->execute();

            echo 'success';

        }else{
        	echo 'error';
        }
        

    }

    public function actionUpdateOrderStatusThirty()
    {
        $orders2 = Yii::$app->db->createCommand('SELECT id, uin, operation, payment_method, day_shipping, order_status FROM orders_stock WHERE payment_method = "30" AND order_status != "CANCELED"')->queryAll();

        foreach ($orders2 as $key => $value) {
                $comp = true;
                if((int)$value['day_shipping'] != 0 && ($value['day_shipping'] != '' || $value['day_shipping'] != 'ERROR' || $value['day_shipping'] != NULL) ){
                    $fN = (strtotime('now')-strtotime($value['day_shipping']))/(60*60*24);
                    $countDays = 31;
                    $comp = ((int)$fN <= $countDays);
                }
                $enableddOperations = array('stock out - to supplier', 'stock out - to customer', 'stock out - after sale replacement', 'stock in - after sale return');
                if(in_array(strtolower($value['operation']), $enableddOperations)){
	                if ($comp == true){
	                    
	                    if($value['day_shipping'] == '' || $value['day_shipping'] == 'ERROR' || $value['day_shipping'] == NULL || $value['day_shipping'] == '0000-00-00'){
	                        $correct = 'IN PROGRESS';
	                    }else{
	                        $correct = 'IN TRANSIT';
	                    }

	                }
	                else{
	                    $correct = 'COMPLETED';
	                }
	            }

                if($value['order_status'] != $correct){
                    $update_order_str .= " WHEN '" . $value['id'] . "' THEN '".addslashes($correct)."' ";
                    $update_order_where_str[] = $value['id'];
                }
                //echo 'correct value for '.$value['id'].' - '.$correct.'(day shipping is '.$value['day_shipping'].')<br>';

        }

        if($update_order_where_str && $update_order_str){
            $update2 = Yii::$app->db->createCommand("
                    UPDATE orders_stock
                    SET order_status = (CASE id $update_order_str END)
                    WHERE id IN('" . implode("','", $update_order_where_str) . "');
                ")->execute();

            //echo 'success';

        }else{
            //echo 'error';
        }
        

    }
    public function actionUpdateOrderStatusSixteen()
    {
        $orders2 = Yii::$app->db->createCommand('SELECT id, uin, operation, payment_method, day_shipping, order_status FROM orders_stock WHERE payment_method = "60" AND order_status != "CANCELED"')->queryAll();

        foreach ($orders2 as $key => $value) {
                $comp = true;
                if((int)$value['day_shipping'] != 0 && ($value['day_shipping'] != '' || $value['day_shipping'] != 'ERROR' || $value['day_shipping'] != NULL) ){
                    $fN = (strtotime('now')-strtotime($value['day_shipping']))/(60*60*24);
                    $countDays = 61;
                    $comp = ((int)$fN <= $countDays);
                }

                $enableddOperations = array('stock out - to supplier', 'stock out - to customer', 'stock out - after sale replacement', 'stock in - after sale return');
                if(in_array(strtolower($value['operation']), $enableddOperations)){
	                if ($comp == true){
	                    
	                    if($value['day_shipping'] == '' || $value['day_shipping'] == 'ERROR' || $value['day_shipping'] == NULL || $value['day_shipping'] == '0000-00-00'){
	                        $correct = 'IN PROGRESS';
	                    }else{
	                        $correct = 'IN TRANSIT';
	                    }

	                }
	                else{
	                    $correct = 'COMPLETED';
	                }
	            }

                if($value['order_status'] != $correct){
                    $update_order_str .= " WHEN '" . $value['id'] . "' THEN '".addslashes($correct)."' ";
                    $update_order_where_str[] = $value['id'];
                }
                //echo 'correct value for '.$value['id'].' - '.$correct.'(day shipping is '.$value['day_shipping'].')<br>';

        }

        if($update_order_where_str && $update_order_str){
            $update2 = Yii::$app->db->createCommand("
                    UPDATE orders_stock
                    SET order_status = (CASE id $update_order_str END)
                    WHERE id IN('" . implode("','", $update_order_where_str) . "');
                ")->execute();

            //echo 'success';

        }else{
            //echo 'error';
        }
        

    }



    public function actionUpdatePendingOrderStatus()
    {
        $orders2 = Yii::$app->db->createCommand('SELECT id, day_shipping, order_status, payment_status_to_chcos, outstanding_payment_to_chcos FROM orders_stock WHERE order_status = "COMPLETED" and operation = "Stock OUT - To Customer" AND (TIMESTAMPDIFF(DAY, day_shipping,NOW()) <= 20)')->queryAll();
        //$payment_status = 'PENDING';
        $order_status = 'IN TRANSIT';
        foreach ($orders2 as $key => $value) {
                    //$update_payment_str .= " WHEN '" . $value['id'] . "' THEN '".addslashes($payment_status)."' ";
                    $update_order_str .= " WHEN '" . $value['id'] . "' THEN '".addslashes($order_status)."' ";
                    $update_order_where_str[] = $value['id'];
        }
        // echo '<pre>';
        // var_dump($orders2);
        // echo '</pre>';
        if($update_order_where_str && $update_order_str){
            $update2 = Yii::$app->db->createCommand("
                    UPDATE orders_stock
                    SET order_status = (CASE id $update_order_str END)
                    WHERE id IN('" . implode("','", $update_order_where_str) . "');
                ")->execute();

            echo 'success';

        }
        echo 'error';

    }

    public function actionUpdatePaymentStatus()
    {
        $orders2 = Yii::$app->db->createCommand('SELECT id, operation,outstanding_payment_to_chcos, payment_status_to_chcos, order_status FROM orders_stock')->queryAll();

        foreach ($orders2 as $key => $value) {

        	$enableddOperations = array('stock out - to supplier', 'stock out - to customer', 'stock out - after sale replacement', 'stock in - after sale return');
            if(in_array(strtolower(trim($value['operation'])), $enableddOperations)){

	            if($value['order_status'] == 'COMPLETED'){
	                if($value['outstanding_payment_to_chcos'] == 0){

	                    $correct = 'COMPLETED';

	                    if($value['payment_status_to_chcos'] != $correct){
	                        $update_payment_str .= " WHEN '" . $value['id'] . "' THEN '".addslashes($correct)."' ";
	                        $update_payment_where_str[] = $value['id'];
	                    }
	                }else if($value['outstanding_payment_to_chcos'] != 0){
	                    $correct = 'IN PROGRESS';
	                    if($value['payment_status_to_chcos'] != $correct){
	                        $update_payment_str .= " WHEN '" . $value['id'] . "' THEN '".addslashes($correct)."' ";
	                        $update_payment_where_str[] = $value['id'];
	                    }
	                }

	            }else{
	                $correct = 'PENDING';
	                if($value['payment_status_to_chcos'] != $correct){
	                        $update_payment_str .= " WHEN '" . $value['id'] . "' THEN '".addslashes($correct)."' ";
	                        $update_payment_where_str[] = $value['id'];
	                    }
	            }
	        }else{
	        	$correct = '';
	        	if($value['payment_status_to_chcos'] != $correct){
	                        $update_payment_str .= " WHEN '" . $value['id'] . "' THEN '".addslashes($correct)."' ";
	                        $update_payment_where_str[] = $value['id'];
	                    }
	        }


        }
        if($update_payment_where_str && $update_payment_str){
            $update2 = Yii::$app->db->createCommand("
                    UPDATE orders_stock
                    SET payment_status_to_chcos = (CASE id $update_payment_str END)
                    WHERE id IN('" . implode("','", $update_payment_where_str) . "');
                ")->execute();

            echo 'success';

        }

        die();

    }

    public function actionRecalculateStock()
    {
        ini_set('memory_limit', '1024M');

        $stock = Yii::$app->db->createCommand("SELECT DISTINCT iar
            FROM orders_stock
            WHERE shipped_in_detail NOT IN ('ZEESHOP', '')
        ")->queryAll();

        foreach ($stock as $key=>$value){
            $currentQ = Yii::$app->db->createCommand("SELECT qty
                FROM stock
                WHERE iar = '".$value['iar']."'
            ")->queryOne();

            $rightQ = Yii::$app->db->createCommand("SELECT SUM(real_quantity) AS quantity
                FROM orders_stock
                WHERE iar='" . $value['iar'] . "' AND (shipped_in_detail != 'ZEESHOP' OR shipped_in_detail IS NULL)
            ")->queryOne();

            if($currentQ['qty'] != $rightQ['quantity'])
            {
                // $update = Yii::$app->db->createCommand("
                //     UPDATE stock
                //     SET qty=" . $rightQ['quantity'] . " ,
                //         stock_value = qty*unit_price,
                //         updated_at = '" . date('Y-m-d H:i:s') . "'
                //     WHERE iar='" . $value['iar'] . "'
                // ")->execute();
                echo 'UPDATED - old quantity for '.$value['iar'].' is '.$currentQ['qty'].' and correct quantity is '.$rightQ['quantity'].'<br>';
            }

        }

        // var_dump(count($stock));

        die();

    }

    public function actionGetAllOrders()
    {
        $_POST['itoken'] = \backend\controllers\FeedController::$itoken;

        $data['itoken'] = \backend\controllers\FeedController::$itoken;

        // curl_setopt($curl, CURLOPT_URL, Yii::$app->params['euMagentoURL'] . '/supplier-dashboard/index/getorders');
         if( $curl = curl_init() ) {

            // curl_setopt($curl, CURLOPT_URL, Yii::$app->params['euMagentoURL'] . '/supplier-dashboard/index/getorders');
            // curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            // curl_setopt($curl, CURLOPT_POST, true);
            // curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            // $out = curl_exec($curl);

            // curl_close($curl);


            // $orders = unserialize($out);

            $rows = (new \yii\db\Query())
                ->select(['iref'])
                ->from('orders_stock')
                //->where('iref LIKE "O%"')
                ->distinct()
                ->all();

            foreach($rows as $row) echo '\''.$row['iref'] . '\',';

            die;

            //echo count($orders);
            // echo '<pre>';
            // var_dump(array_udiff($orders, $rows, 'compare_ids'));
            // echo '</pre>';

            //die();





            // foreach ($customerOrderCompany as $singleOrder)
            //     {
            //         $update_buyer_str .= " WHEN '" . $singleOrder['id'] . "' THEN '".addslashes($singleOrder['company'])."' ";
            //         $update_where_str[] = $singleOrder['id'];
            //        // $log_batch[] = [0, NULL, $row['iref'] . '(' . $row['iar'] . ') - COMPLETED'];
            //     }

            // $update1 = Yii::$app->db->createCommand("
            //     UPDATE orders_stock
            //     SET buyer = (CASE iref $update_buyer_str END),
            //         updated_at = '" . date('Y-m-d H:i:s') . "'
            //     WHERE iref IN('" . implode("','", $update_where_str) . "');
            // ")->execute();




            // $items_arr = Yii::$app->db->createCommand("SELECT buyer_id, buyer FROM orders_stock WHERE iref IN('" . implode("','", $update_where_str) . "')")->queryAll();

            // foreach ($items_arr as $item)
            //     {
            //         $update_company_str .= " WHEN '" . $item['id'] . "' THEN '".addslashes($item['buyer'])."' ";
            //         $update_company_where_str[] = $item['id'];
            //        // $log_batch[] = [0, NULL, $row['iref'] . '(' . $row['iar'] . ') - COMPLETED'];
            //     }
            //     //echo $update_company_str;
            // $update2 = Yii::$app->db->createCommand("
            //     UPDATE buyers
            //     SET name = (CASE id $update_company_str END)
            //     WHERE id IN('" . implode("','", $update_company_where_str) . "');
            // ")->execute();

            // echo 'success!';
        }
    }

    public function actionUpdateNameOrders()
    {
        //var_dump('123123');
        //die();
        $_POST['itoken'] = 'lsYtKjzGDDYHHB9FHjsrxiY5z2aIlOIF';

        $data['itoken'] = \backend\controllers\FeedController::$itoken;

        // curl_setopt($curl, CURLOPT_URL, Yii::$app->params['euMagentoURL'] . '/supplier-dashboard/index/getorders');
         if( $curl = curl_init() ) {

            curl_setopt($curl, CURLOPT_URL, Yii::$app->params['euMagentoURL'] . '/en/supplier-dashboard/index/getcompanies');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $out = curl_exec($curl);

            curl_close($curl);

            //var_dump($out);

            $companies = unserialize($out);
            // var_dump(count($customerOrderCompany));
            // die();

            foreach ($companies as $company)
                {
                    $update_buyer_str .= " WHEN '" . $company['sku'] . "' THEN '".addslashes($company['name'])."' ";
                    $update_where_str[] = $company['sku'];
                }

            if ($update1 = Yii::$app->db->createCommand("
                UPDATE `users`
                SET user_name = (CASE user_code $update_buyer_str END)
                WHERE user_code IN('" . implode("','", $update_where_str) . "');
            ")->execute())
                echo 'table _buyers_ updated successfully!<br>';
            else
                echo 'table _buyers_ not updated!<br>';

            

            if ($update2 = Yii::$app->db->createCommand("
                UPDATE order_stock JOIN users 
                    ON order_stock.user_id = users.id
                SET order_stock.seller = users.user_name
            ")->execute())
                echo 'table _order_stock_ updated successfully!<br>';
            else
                echo 'table _order_stock_ not updated!<br>';





            // $items_arr = Yii::$app->db->createCommand("SELECT buyer_id, buyer_name FROM orders_stock WHERE iref IN('" . implode("','", $update_where_str) . "') AND buyer_id IS NOT NULL")->queryAll();
            // foreach ($items_arr as $item)
            //     {
            //         $update_company_str .= " WHEN '" . $item['buyer_id'] . "' THEN '".addslashes($item['buyer_name'])."' ";
            //         $update_company_where_str[] = addslashes($item['buyer_id']);
            //     }
            //     var_dump("
            //     UPDATE `buyers`
            //     SET buyer_name = (CASE id $update_company_str END)
            //     WHERE id IN('" . implode("','", $update_company_where_str) . "');
            // // ");
            //     //echo $update_company_str;
            // if($update2 = Yii::$app->db->createCommand("
            //     UPDATE `buyers`
            //     SET buyer_name = (CASE id $update_company_str END)
            //     WHERE id IN('" . implode("','", $update_company_where_str) . "');
            // ")->execute())
            //         echo 'update 2 success<br>';
            //     else
            //         echo 'update 2 error<br>';

            // echo 'success!';
        }
    }



    public function actionUpdateDayShipping()
    {
        $_POST['itoken'] = 'lsYtKjzGDDYHHB9FHjsrxiY5z2aIlOIF';

        $data['itoken'] = \backend\controllers\FeedController::$itoken;

         if( $curl = curl_init() ) {

            curl_setopt($curl, CURLOPT_URL, Yii::$app->params['euMagentoURL'] . '/en/supplier-dashboard/index/getdayshipping');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            $out = curl_exec($curl);
            curl_close($curl);

            $dayshippings = unserialize($out);

             echo '<pre>';
             var_dump(count($dayshippings));
             echo '</pre>';

            foreach ($dayshippings as $day)
            {
                $update_day_str .= " WHEN '" . $day['order_id'] . "' THEN '".addslashes($day['day_shipping'])."' ";
                $update_where_str[] = $day['order_id'];
            }

            if ($update = Yii::$app->db->createCommand("
            UPDATE `orders_stock`
            SET day_shipping = (CASE iref $update_day_str END),
                updated_at = '" . date('Y-m-d H:i:s') . "'
                WHERE iref IN('" . implode("','", $update_where_str) . "')
                AND (day_shipping is NULL OR day_shipping ='0' OR day_shipping ='0000-00-00' OR day_shipping = '0000-00-00 00:00:00');
            ")->execute())
                echo 'update success!<br>';
            else
                echo 'update error!<br>';
        }
    }


}
